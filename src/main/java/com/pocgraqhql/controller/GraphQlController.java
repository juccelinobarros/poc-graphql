package com.pocgraqhql.controller;

import com.pocgraqhql.exception.CustomException;
import com.pocgraqhql.graphql.GraphqlProvider;
import graphql.ExecutionResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/queries")
@RequiredArgsConstructor
@Slf4j
public class GraphQlController {

    private final GraphqlProvider graphqlProvider;

    @PostMapping
    public ResponseEntity<Object> executeQuery(@RequestBody String query) {
        long startTime = System.currentTimeMillis();
        ExecutionResult result = this.graphqlProvider.graphQL().execute(query);
        log.info("*** Time Execution (ms): "+ (System.currentTimeMillis() - startTime));
        checkIfErrorExecution(result);
        return ResponseEntity.ok(result);
    }

    private void checkIfErrorExecution(ExecutionResult result) {
        if (!result.getErrors().isEmpty()) {
            /* Creating a specific response body for the error */
            throw new CustomException(result.getErrors().get(0).getExtensions().get("message").toString(),
                    result.getErrors().get(0).getExtensions().get("field").toString(),
                    Integer.valueOf(result.getErrors().get(0).getExtensions().get("httpStatus").toString()));
        }
    }
}
