package com.pocgraqhql.config;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import springfox.documentation.spring.web.paths.AbstractPathProvider;

@AllArgsConstructor
@NoArgsConstructor
public class BasePathProvider extends AbstractPathProvider {

    private String baseUrl;

    @Override
    protected String applicationPath() {
        return baseUrl;
    }

    @Override
    protected String getDocumentationPath() {
        return "/";
    }
}
