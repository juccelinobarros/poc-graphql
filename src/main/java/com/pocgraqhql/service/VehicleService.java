package com.pocgraqhql.service;

import com.pocgraqhql.dao.VehicleRepository;
import com.pocgraqhql.entity.Vehicle;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class VehicleService {

    private final VehicleRepository vehicleRepository;


    public List<Vehicle> getAll() {
        return this.vehicleRepository.findAll();
    }

    public List<Vehicle> findAll(final int count) {
        return this.vehicleRepository.findAll().stream().limit(count).collect(Collectors.toList());
    }

    public Optional<Vehicle> findById(final int id) {
        return this.vehicleRepository.findById(id);
    }

    public List<Vehicle> findByIds(final List<Integer> ids) {
        return this.vehicleRepository.findAllById(ids);
    }

    public Vehicle saveOrUpdate(final Vehicle vehicle) {
        return this.vehicleRepository.save(vehicle);
    }

    public void remove(final Vehicle vehicle) {
        this.vehicleRepository.delete(vehicle);
    }

    public void removeById(final Integer id) {
        this.vehicleRepository.deleteById(id);
    }

}
