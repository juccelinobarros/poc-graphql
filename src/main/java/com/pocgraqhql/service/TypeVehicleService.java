package com.pocgraqhql.service;

import graphql.com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class TypeVehicleService {

    /**
     * Load all Types
     * Mocking an external API
     * @return
     */
    public List<Map<String, Object>> getAllVehicleTypes() throws InterruptedException {
        Thread.sleep(2000);
        return Arrays.asList(
                ImmutableMap.of("id", 1,
                        "typeName", "Car"),
                ImmutableMap.of("id", 2,
                        "typeName", "Bus"),
                ImmutableMap.of("id", 3,
                        "typeName", "Bike")
        );
    }

}
