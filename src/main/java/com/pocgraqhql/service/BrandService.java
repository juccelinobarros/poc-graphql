package com.pocgraqhql.service;

import graphql.com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@Slf4j
public class BrandService {

    /**
     * Load all brands
     * Mocking an external API
     * @return
     */
    public List<Map<String, String>> getAllBrands() throws InterruptedException {
        Thread.sleep(5000);
        return Arrays.asList(
                ImmutableMap.of("id", "brand1",
                        "name", "Fiat",
                        "country", "Italy"),
                ImmutableMap.of("id", "brand2",
                        "name", "BMW",
                        "country", "Germany"),
                ImmutableMap.of("id", "brand3",
                        "name", "Renault",
                        "country", "France")
        );
    }

    public Optional<Map<String, String>> getBrandById(String brandId) throws InterruptedException {
        return this.getAllBrands().stream()
                .filter(brand -> brand.get("id").equals(brandId))
                .findFirst();
    }

    public Boolean checkBrandIfExistsById(String brandId) throws InterruptedException {
        return this.getAllBrands()
                .stream()
                .anyMatch(brand -> brand.get("id").equals(brandId));
    }

}
