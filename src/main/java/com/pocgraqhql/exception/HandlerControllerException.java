package com.pocgraqhql.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@RestControllerAdvice
public class HandlerControllerException extends ResponseEntityExceptionHandler {

    /**
     * Handle response error with http status code according to Custom Exception.
     * @return ResponseEntity with body message
     */
    @ExceptionHandler(CustomException.class)
    public ResponseEntity<Object> invalidInputException(CustomException customException) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", customException.getMessage());
        body.put("invalidField", customException.getInvalidField());

        return new ResponseEntity<>(body, HttpStatus.valueOf(customException.getHttpStatus()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> anyExceptionHandler() {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("message", "Internal Server Error");

        return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
