package com.pocgraqhql.exception;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import lombok.Getter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Getter
public class CustomException extends RuntimeException implements GraphQLError {

    private final String invalidField;
    private final Integer httpStatus;

    public CustomException(String message, String invalidField, Integer httpStatus) {
        super(message);
        this.invalidField = invalidField;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public Map<String, Object> getExtensions() {
        Map<String, Object> customAttributes = new LinkedHashMap<>();
        customAttributes.put("message", this.getMessage());
        customAttributes.put("field", this.invalidField);
        customAttributes.put("httpStatus", this.httpStatus);
        return customAttributes;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.DataFetchingException;
    }
}


