package com.pocgraqhql.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class Vehicle {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "type_id", nullable = false)
    private Integer typeId;

    @Column(name = "model_name", nullable = false)
    private String modelName;

    @Column(name = "brand_id", nullable = false)
    private String brandId;

    @Column(name = "launch_date")
    private LocalDate launchDate;

    public Vehicle(Integer id, Integer typeId, String modelName, String brandId, LocalDate launchDate) {
        this.id = id;
        this.typeId = typeId;
        this.modelName = modelName;
        this.brandId = brandId;
        this.launchDate = launchDate;
    }

    public Vehicle() {

    }
}
