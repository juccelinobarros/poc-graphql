package com.pocgraqhql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocGraqhqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocGraqhqlApplication.class, args);
	}

}
