package com.pocgraqhql.graphql;

import com.pocgraqhql.service.BrandService;
import com.pocgraqhql.service.VehicleService;
import graphql.schema.DataFetcher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BrandDataFetcher {

    private final BrandService brandService;
    private final VehicleService vehicleService;

    public DataFetcher vehiclesByBrand() {
        return dataFetchingEnvironment -> {
            String brandId = dataFetchingEnvironment.getArgument("brandId");
            return this.brandService.getBrandById(brandId).orElse(null);
        };
    }

    public DataFetcher getVehiclesBrandDataFetcher() {
        return dataFetchingEnvironment -> {
            Map<String, String> brand = dataFetchingEnvironment.getSource();
            return this.vehicleService.getAll()
                    .stream()
                    .filter(vehicle -> vehicle.getBrandId().equals(brand.get("id")))
                    .collect(Collectors.toList());
        };
    }

}
