package com.pocgraqhql.graphql;

import com.pocgraqhql.entity.Vehicle;
import com.pocgraqhql.exception.CustomException;
import com.pocgraqhql.service.BrandService;
import com.pocgraqhql.service.TypeVehicleService;
import com.pocgraqhql.service.VehicleService;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.xml.bind.ValidationException;
import java.time.LocalDate;
import java.util.concurrent.CompletableFuture;

@Component
@RequiredArgsConstructor
@Slf4j
public class VehicleDataFetcher {

    private final VehicleService vehicleService;
    private final BrandService brandService;
    private final TypeVehicleService typeVehicleService;

    public DataFetcher getAllVehiclesDataFetcher() {
        return dataFetchingEnvironment -> {
            Integer count = dataFetchingEnvironment.getArgument("count");
            // From DB
            return this.vehicleService.findAll(count);
        };
    }

    public DataFetcher getVehicleByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            // From DB
            return this.vehicleService.findById(dataFetchingEnvironment.getArgument("id"));
        };
    }

    /**
     * Mapping the brand which belong to a specific vehicle
     * @return Brand
     */
    public DataFetcher getBrandDataFetcher() {
        return dataFetchingEnvironment -> {
            Vehicle vehicle = dataFetchingEnvironment.getSource();
            // From External API (Mocking)
            return this.brandService.getAllBrands()
                    .stream()
                    .filter(brand -> brand.get("id").equals(vehicle.getBrandId()))
                    .findFirst()
                    .orElse(null);
        };
    }

    /**
     * Mapping the type which belong to a specific vehicle
     * @return Type
     */
    public DataFetcher getVehicleTypeDataFetcher() {
        return dataFetchingEnvironment -> {
            Vehicle vehicle = dataFetchingEnvironment.getSource();
            // From External API (Mocking)
            return this.typeVehicleService.getAllVehicleTypes()
                    .stream()
                    .filter(type -> type.get("id") == vehicle.getTypeId())
                    .findFirst()
                    .orElse(null);
        };
    }


    /*
     * ASYNC Methods
     */
    public DataFetcher getAllVehiclesDataFetcherAsync() {
        return environment -> CompletableFuture.supplyAsync(() ->
                this.vehicleService.findAll(environment.getArgument("count")));
    }

    public DataFetcher getVehicleByIdDataFetcherAsync() {
        return environment -> CompletableFuture.supplyAsync(() ->
                this.vehicleService.findById(environment.getArgument("id")));
    }

    public DataFetcher getVehiclesByIdsDataFetcherAsync() {
        return environment -> CompletableFuture.supplyAsync(() ->
                this.vehicleService.findByIds(environment.getArgument("ids")));
    }

    /**
     * Mapping the brand which belong to a specific vehicle ASYNC
     * @return Brand
     */
    public DataFetcher getBrandDataFetcherAsync() {
        return environment -> CompletableFuture.supplyAsync(() -> {
            Vehicle vehicle = environment.getSource();
            try {
                return this.brandService.getAllBrands()
                        .stream()
                        .filter(brand -> brand.get("id").equals(vehicle.getBrandId()))
                        .findFirst()
                        .orElse(null);
            } catch (InterruptedException e) {
                throw new CustomException("Error while fetching Brand data", "none", HttpStatus.INTERNAL_SERVER_ERROR.value());
            }
        });
    }

    /**
     * Mapping the brand which belong to a specific vehicle ASYNC
     * @return Brand
     */
    public DataFetcher getTypeDataFetcherAsync() {
        return environment -> CompletableFuture.supplyAsync(() -> {
            Vehicle vehicle = environment.getSource();
            try {
                return this.typeVehicleService.getAllVehicleTypes()
                        .stream()
                        .filter(type -> type.get("id") == vehicle.getTypeId())
                        .findFirst()
                        .orElse(null);
            } catch (InterruptedException e) {
                throw new CustomException("Error while fetching Type data", "none", HttpStatus.INTERNAL_SERVER_ERROR.value());
            }
        });
    }


    /*
     * MUTATION QUERIES
     */

    public DataFetcher removeVehicleByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            Integer vehicleId = dataFetchingEnvironment.getArgument("id");
            if (vehicleId == null) {
                /* handling a custom exception */
                throw new CustomException("Vehicle id must not be null", "id", HttpStatus.BAD_REQUEST.value());
            }
            this.vehicleService.findById(vehicleId).ifPresent(this.vehicleService::remove);
            return true;
        };
    }

    public DataFetcher saverOrUpdateVehicle() {
        return dataFetchingEnvironment -> {
            String brandID = dataFetchingEnvironment.getArgument("brandId");
            if (this.brandService.checkBrandIfExistsById(brandID)) {
                return this.persistVehicle(dataFetchingEnvironment, brandID);
            }
            throw new ValidationException("Brand not exists");
        };
    }

    private Vehicle persistVehicle(DataFetchingEnvironment dataFetchingEnvironment, String brandID) {
        Vehicle vehicle = new Vehicle(
                dataFetchingEnvironment.getArgument("id"),
                dataFetchingEnvironment.getArgument("type"),
                dataFetchingEnvironment.getArgument("modelName"),
                brandID,
                LocalDate.parse(dataFetchingEnvironment.getArgument("launchDate"))
        );
        return this.vehicleService.saveOrUpdate(vehicle);
    }


}
