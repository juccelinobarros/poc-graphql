package com.pocgraqhql.graphql;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.schema.idl.TypeRuntimeWiring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Component
public class GraphqlProvider {

    @Autowired
    VehicleDataFetcher vehicleDataFetcher;
    @Autowired
    BrandDataFetcher brandDataFetcher;

    private GraphQL graphQL;

    @Value("classpath:graphql/schema.graphqls")
    Resource resource;

    @Bean
    public GraphQL graphQL() {
        return this.graphQL;
    }

    @PostConstruct
    public void init() throws IOException, InterruptedException {
        File schemaFile = this.resource.getFile();
        GraphQLSchema graphQLSchema = buildSchema(schemaFile);
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private GraphQLSchema buildSchema(File schemaFile) throws InterruptedException {
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(schemaFile);
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
    }

    private RuntimeWiring buildWiring() throws InterruptedException {
        return RuntimeWiring.newRuntimeWiring()
                /* Query Methods */
                .type(TypeRuntimeWiring.newTypeWiring("Query")
                        .dataFetcher("vehicleById", this.vehicleDataFetcher.getVehicleByIdDataFetcher())
                        .dataFetcher("vehicles", this.vehicleDataFetcher.getAllVehiclesDataFetcher())
                        .dataFetcher("vehiclesByBrand", this.brandDataFetcher.vehiclesByBrand())
                        /* ASYNC */
                        .dataFetcher("vehiclesAsync", this.vehicleDataFetcher.getAllVehiclesDataFetcherAsync())
                        .dataFetcher("vehicleByIdAsync", this.vehicleDataFetcher.getVehicleByIdDataFetcherAsync())
                        .dataFetcher("vehicleByIdsAsync", this.vehicleDataFetcher.getVehiclesByIdsDataFetcherAsync()))

                /* Mutation Methods */
                .type(TypeRuntimeWiring.newTypeWiring("Mutation")
                        .dataFetcher("removeVehicle", this.vehicleDataFetcher.removeVehicleByIdDataFetcher())
                        .dataFetcher("saveOrUpdateVehicle", this.vehicleDataFetcher.saverOrUpdateVehicle())
                )
                /* Mapping Data Relation */
                .type(TypeRuntimeWiring.newTypeWiring("Vehicle")
                        .dataFetcher("brand", this.vehicleDataFetcher.getBrandDataFetcher())
                        .dataFetcher("type", this.vehicleDataFetcher.getVehicleTypeDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Brand")
                        .dataFetcher("vehicles", this.brandDataFetcher.getVehiclesBrandDataFetcher()))
                /* ASYNC */
                .type(TypeRuntimeWiring.newTypeWiring("VehicleAsync")
                        .dataFetcher("brand", this.vehicleDataFetcher.getBrandDataFetcherAsync())
                        .dataFetcher("type", this.vehicleDataFetcher.getTypeDataFetcherAsync()))
                .build();
    }
}
