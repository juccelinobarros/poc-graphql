-- VEHICLES
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (994, 1, 'I4', 'brand2', '2018-06-15');
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (995, 1, 'Palio', 'brand1', '2017-03-21');
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (996, 3, 'Sandero', 'brand3', '2015-12-31');
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (997, 2, 'Berlina', 'brand2', '2014-11-15');
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (998, 3, 'Sandero Step Way', 'brand3', '2019-10-25');
INSERT INTO vehicle(id, type_id, model_name, brand_id, launch_date) VALUES (999, 1, 'Punto', 'brand1', '2020-05-24');
