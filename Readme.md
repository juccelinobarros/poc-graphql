## Examples of Graphql:s

URL:
**http://localhost:8080/graphiql**


**Get All Vehicles (pagination)**

`{
vehicles(count: 5) {
id
modelName
brand {
name
country
}
}
}`

**Get Vehicle by Id**

`{
vehicleById(id: 999) {
id
modelName
brand {
name
country
}
}
}`

**Save or Update Vehicle**

`mutation {
saveOrUpdateVehicle(
id:995, // if null -> new Vehicle
type: "Bus",
modelName: "Auto Bus",
brandId: "brand1",
launchDate: "2021-12-11")
{
id
brand {
country
}
}
}
`

**Remove by Id**

`mutation {
removeVehicle(id: 994)
}`

**Get Vehicles by Brand Id**

`{ vehiclesByBrand(brandId: "brand1") { id name vehicles { type modelName } } }`